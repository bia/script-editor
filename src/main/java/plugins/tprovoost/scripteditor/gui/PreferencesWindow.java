package plugins.tprovoost.scripteditor.gui;

import icy.gui.frame.IcyFrame;
import icy.preferences.GeneralPreferences;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.EventListenerList;

public class PreferencesWindow extends IcyFrame
{
    private static PreferencesWindow singleton = new PreferencesWindow();

    private Preferences preferences = Preferences.getPreferences();

    private JSpinner spinFontSize;
    private JSpinner tfSpacesTab;
    private JCheckBox cboxSoft;
    private JCheckBox cboxAutoClearOutput;
    private JCheckBox cboxFullAutocomplete;

    private PreferencesWindow()
    {
        super("Script Editor Preferences", true, true, false, true);
        
        setContentPane(createGUI());
    }

    public static PreferencesWindow getPreferencesWindow()
    {
        return singleton;
    }

    private JPanel createGUI()
    {
        JPanel toReturn = new JPanel();
        toReturn.setBorder(new EmptyBorder(4, 4, 4, 4));
        toReturn.setLayout(new BorderLayout(0, 0));

        JLabel lblPreferences = new JLabel("<html><h2>Preferences</h2></html>");
        lblPreferences.setHorizontalAlignment(SwingConstants.CENTER);
        toReturn.add(lblPreferences, BorderLayout.NORTH);

        JPanel panelCenter = new JPanel();
        toReturn.add(panelCenter, BorderLayout.CENTER);

        GridBagLayout gbl_panel_1 = new GridBagLayout();
        gbl_panel_1.columnWidths = new int[] {0, 0, 0};
        gbl_panel_1.rowHeights = new int[] {0, 0, 0, 0, 0, 0};
        gbl_panel_1.columnWeights = new double[] {1.0, 0.0, Double.MIN_VALUE};
        gbl_panel_1.rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelCenter.setLayout(gbl_panel_1);
        JLabel lblFullAutocomplete = new JLabel("Enable 'auto complete' after any character typed");
        GridBagConstraints gbc_lblFullAutocomplete = new GridBagConstraints();
        gbc_lblFullAutocomplete.anchor = GridBagConstraints.WEST;
        gbc_lblFullAutocomplete.insets = new Insets(0, 0, 5, 5);
        gbc_lblFullAutocomplete.gridx = 0;
        gbc_lblFullAutocomplete.gridy = 0;
        panelCenter.add(lblFullAutocomplete, gbc_lblFullAutocomplete);
        cboxFullAutocomplete = new JCheckBox("");
        GridBagConstraints gbc_cboxFullAutocomplete = new GridBagConstraints();
        gbc_cboxFullAutocomplete.anchor = GridBagConstraints.WEST;
        gbc_cboxFullAutocomplete.insets = new Insets(0, 0, 5, 0);
        gbc_cboxFullAutocomplete.gridx = 1;
        gbc_cboxFullAutocomplete.gridy = 0;
        panelCenter.add(cboxFullAutocomplete, gbc_cboxFullAutocomplete);

        JLabel lblAutoClearOutput = new JLabel("Always clear the output when run is clicked");
        GridBagConstraints gbc_lblAutoClearOutput = new GridBagConstraints();
        gbc_lblAutoClearOutput.anchor = GridBagConstraints.WEST;
        gbc_lblAutoClearOutput.insets = new Insets(0, 0, 5, 5);
        gbc_lblAutoClearOutput.gridx = 0;
        gbc_lblAutoClearOutput.gridy = 1;
        panelCenter.add(lblAutoClearOutput, gbc_lblAutoClearOutput);

        cboxAutoClearOutput = new JCheckBox("");
        GridBagConstraints gbc_cboxAutoClearOutput = new GridBagConstraints();
        gbc_cboxAutoClearOutput.anchor = GridBagConstraints.WEST;
        gbc_cboxAutoClearOutput.insets = new Insets(0, 0, 5, 0);
        gbc_cboxAutoClearOutput.gridx = 1;
        gbc_cboxAutoClearOutput.gridy = 1;
        panelCenter.add(cboxAutoClearOutput, gbc_cboxAutoClearOutput);

        JLabel lblSoft = new JLabel("Use Soft tabs");
        lblSoft.setToolTipText("Auto-expand tabs to spaces");
        GridBagConstraints gbc_lblSoft = new GridBagConstraints();
        gbc_lblSoft.anchor = GridBagConstraints.WEST;
        gbc_lblSoft.insets = new Insets(0, 0, 5, 5);
        gbc_lblSoft.gridx = 0;
        gbc_lblSoft.gridy = 2;
        panelCenter.add(lblSoft, gbc_lblSoft);

        cboxSoft = new JCheckBox("");
        GridBagConstraints gbc_cboxSoft = new GridBagConstraints();
        gbc_cboxSoft.anchor = GridBagConstraints.WEST;
        gbc_cboxSoft.insets = new Insets(0, 0, 5, 0);
        gbc_cboxSoft.gridx = 1;
        gbc_cboxSoft.gridy = 2;
        panelCenter.add(cboxSoft, gbc_cboxSoft);

        JLabel lblSpacesTab = new JLabel("Tab width");
        GridBagConstraints gbc_lblSpacesTab = new GridBagConstraints();
        gbc_lblSpacesTab.anchor = GridBagConstraints.WEST;
        gbc_lblSpacesTab.insets = new Insets(0, 0, 5, 5);
        gbc_lblSpacesTab.gridx = 0;
        gbc_lblSpacesTab.gridy = 3;
        panelCenter.add(lblSpacesTab, gbc_lblSpacesTab);

        tfSpacesTab = new JSpinner();
        tfSpacesTab.setModel(new SpinnerNumberModel(4, 1, 10, 1));
        GridBagConstraints gbc_tfSpacesTab = new GridBagConstraints();
        gbc_tfSpacesTab.fill = GridBagConstraints.HORIZONTAL;
        gbc_tfSpacesTab.insets = new Insets(0, 0, 5, 0);
        gbc_tfSpacesTab.gridx = 1;
        gbc_tfSpacesTab.gridy = 3;
        panelCenter.add(tfSpacesTab, gbc_tfSpacesTab);

        JLabel lblFontSize = new JLabel("Font size");
        GridBagConstraints gbc_lblFontSize = new GridBagConstraints();
        gbc_lblFontSize.anchor = GridBagConstraints.WEST;
        gbc_lblFontSize.insets = new Insets(0, 0, 0, 5);
        gbc_lblFontSize.gridx = 0;
        gbc_lblFontSize.gridy = 4;
        panelCenter.add(lblFontSize, gbc_lblFontSize);
        spinFontSize = new JSpinner(new SpinnerNumberModel(GeneralPreferences.getGuiFontSize(), 4, 30, 1));
        GridBagConstraints gbc_spinFontSize = new GridBagConstraints();
        gbc_spinFontSize.fill = GridBagConstraints.HORIZONTAL;
        gbc_spinFontSize.gridx = 1;
        gbc_spinFontSize.gridy = 4;
        panelCenter.add(spinFontSize, gbc_spinFontSize);

        JPanel panelButton = new JPanel();
        toReturn.add(panelButton, BorderLayout.SOUTH);
        GridBagLayout gbl_panelButton = new GridBagLayout();
        gbl_panelButton.columnWidths = new int[] {131, 59, 45, 65, 0};
        gbl_panelButton.rowHeights = new int[] {23, 0};
        gbl_panelButton.columnWeights = new double[] {1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panelButton.rowWeights = new double[] {0.0, Double.MIN_VALUE};
        panelButton.setLayout(gbl_panelButton);

        JButton btnApply = new JButton("Apply");
        GridBagConstraints gbc_btnApply = new GridBagConstraints();
        gbc_btnApply.anchor = GridBagConstraints.NORTH;
        gbc_btnApply.insets = new Insets(0, 0, 0, 5);
        gbc_btnApply.gridx = 1;
        gbc_btnApply.gridy = 0;
        panelButton.add(btnApply, gbc_btnApply);
        btnApply.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                applyPreferences();
            }
        });

        JButton btnOk = new JButton("Ok");
        GridBagConstraints gbc_btnOk = new GridBagConstraints();
        gbc_btnOk.anchor = GridBagConstraints.NORTH;
        gbc_btnOk.insets = new Insets(0, 0, 0, 5);
        gbc_btnOk.gridx = 2;
        gbc_btnOk.gridy = 0;
        panelButton.add(btnOk, gbc_btnOk);
        btnOk.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                applyPreferences();
                close();
            }
        });

        JButton btnCancel = new JButton("Cancel");
        GridBagConstraints gbc_btnCancel = new GridBagConstraints();
        gbc_btnCancel.anchor = GridBagConstraints.NORTH;
        gbc_btnCancel.gridx = 3;
        gbc_btnCancel.gridy = 0;
        panelButton.add(btnCancel, gbc_btnCancel);
        btnCancel.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                // restore the values
                initGUI();
                // do not save and do not tell listeners
                close();
            }
        });

        initGUI();

        return toReturn;
    }

    void initGUI()
    {
        // cboxVarInterp.setSelected(preferences.isVarInterpretationEnabled());
        // cboxOverride.setSelected(preferences.isOverrideEnabled());
        // cboxAutoVerif.setSelected(preferences.isAutoBuildEnabled());
        // cboxStrict.setSelected(preferences.isStrictModeEnabled());
        cboxFullAutocomplete.setSelected(preferences.isFullAutoCompleteEnabled());
        cboxAutoClearOutput.setSelected(preferences.isAutoClearOutputEnabled());
        cboxSoft.setSelected(preferences.isSoftTabsEnabled());
        tfSpacesTab.setValue(Integer.valueOf(preferences.indentSpacesCount()));
        spinFontSize.setValue(Integer.valueOf(preferences.getFontSize()));
    }

    void applyPreferences()
    {
        preferences.setFullAutoCompleteEnabled(cboxFullAutocomplete.isSelected());
        preferences.setAutoClearOutputEnabled(cboxAutoClearOutput.isSelected());
        preferences.setSoftTabsEnabled(cboxSoft.isSelected());
        preferences.setTabWidth(((Integer) tfSpacesTab.getValue()).intValue());
        preferences.setFontSize(((Integer) spinFontSize.getValue()).intValue());

        preferences.savePrefs();
        firePreferencesChanged();
    }

    private final EventListenerList listeners = new EventListenerList();

    public void addPreferencesListener(PreferencesListener listener)
    {
        listeners.add(PreferencesListener.class, listener);
    }

    public void removePreferencesListener(PreferencesListener listener)
    {
        listeners.remove(PreferencesListener.class, listener);
    }

    protected void firePreferencesChanged()
    {
        for (PreferencesListener listener : getPreferencesListeners())
        {
            listener.preferencesChanged();
        }
    }

    public PreferencesListener[] getPreferencesListeners()
    {
        return listeners.getListeners(PreferencesListener.class);
    }
}
