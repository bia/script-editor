/*
 * Copyright 2010-2015 Institut Pasteur.
 * 
 * This file is part of Icy.
 * 
 * Icy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Icy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Icy. If not, see <http://www.gnu.org/licenses/>.
 */
package plugins.tprovoost.scripteditor.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngineManager;

import icy.file.FileUtil;
import icy.gui.frame.IcyFrameAdapter;
import icy.gui.frame.IcyFrameEvent;
import icy.gui.frame.progress.FailedAnnounceFrame;
import icy.main.Icy;
import icy.plugin.PluginLoader;
import icy.plugin.abstract_.PluginActionable;
import icy.system.IcyExceptionHandler;
import icy.system.thread.ThreadUtil;
import plugins.tprovoost.scripteditor.gui.ScriptingEditor;
import plugins.tprovoost.scripteditor.gui.ScriptingPanel;
import plugins.tprovoost.scripteditor.scriptinghandlers.ScriptEngineHandler;
import plugins.tprovoost.scripteditor.scriptinghandlers.ScriptingHandler;

public class ScriptEditorPlugin extends PluginActionable
{
    static List<ScriptingEditor> editors = new ArrayList<ScriptingEditor>();

    @Override
    public void run()
    {
        if (ScriptEngineHandler.getEngineManager().getEngineFactories().isEmpty())
        {
            new FailedAnnounceFrame("No interpreter found. Impossible to compile/run a script.");
            System.out.println("No interpreter found. Impossible to compile/run a script.");
            return;
        }

        final ScriptingEditor frame = new ScriptingEditor();
        frame.setSize(500, 500);
        frame.addToDesktopPane();
        frame.setVisible(true);
        frame.requestFocus();

        editors.add(frame);
        frame.addFrameListener(new IcyFrameAdapter()
        {
            @Override
            public void icyFrameClosed(IcyFrameEvent e)
            {
                frame.removeFrameListener(this);
                editors.remove(frame);
            }
        });

        // get command line arguments
        final String[] args = Icy.getCommandLinePluginArgs();

        // verify that arguments exists and are not 'consumed'
        if ((args.length > 0) && (args[0] != null))
        {
            try
            {
                if (FileUtil.exists(args[0]))
                {
                    // open the script
                    frame.openFile(new File(args[0]));
                    // retrieve the scripting panel
                    final ScriptingPanel pane = (ScriptingPanel) frame.getTabbedPane().getSelectedComponent();
                    final ScriptingHandler handler = pane.getScriptHandler();

                    if (handler != null)
                    {
                        handler.setNewEngine(true);
                        handler.setForceRun(false);
                        handler.setStrict(true);
                        handler.setVarInterpretation(true);
                        handler.interpret(true);
                    }
                }
            }
            catch (Exception e)
            {
                System.err.println("Script Editor: cannot execute specified '" + args[0] + "' script file");
                IcyExceptionHandler.showErrorMessage(e, true);
            }

            // argument consumed
            args[0] = null;
        }
    }

    // @BindingFunction(value = "displayHelloString")
    // public static void displayHello(String h)
    // {
    // System.out.println("Hello " + h + "!");
    // }
    //
    // @BindingFunction(value = "displayHelloInt")
    // public void displayHello(Object a)
    // {
    // System.out.println("Hello " + a + "!");
    // }
    //
    // @BindingFunction(value = "getThisSEP")
    // public ScriptEditorPlugin getThis(String lol)
    // {
    // return this;
    // }
    //
    // @BindingFunction(value = "fnTest")
    // public static int testFuncStatic()
    // {
    // return 42;
    // }

    /**
     * Creates a new tab from a file.
     * 
     * @param f
     *        input file
     * @throws IOException
     *         if the file cannot be opened
     */
    public static void openInScriptEditor(final File f) throws IOException
    {
        // TODO gen text
        BufferedReader reader = new BufferedReader(new FileReader(f));
        String text = "";
        String line;
        while ((line = reader.readLine()) != null)
        {
            text += (line + "\n");
        }
        reader.close();
        openInScriptEditor(text, f.getName());
    }

    /**
     * Creates a new tab containing the <code>text</code>, with no title.
     * 
     * @param text
     *        the input script in String format
     */
    public static void openInScriptEditor(final String text)
    {
        openInScriptEditor(text, "Untitled*");
    }

    /**
     * Creates a new tab with the <code>title</code> and containing the <code>text</code>.
     * 
     * @param text
     *        the input script in String format
     * @param title
     *        tab title
     */
    public static void openInScriptEditor(final String text, final String title)
    {
        if (!editors.isEmpty())
        {
            ThreadUtil.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    ScriptingPanel panel = editors.get(0).createNewPane(title);
                    panel.getTextArea().setText(text);
                }
            });

        }
        else
        {
            ThreadUtil.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    new ScriptEditorPlugin().run();
                    ScriptingPanel panel = editors.get(0).createNewPane(title);
                    panel.getTextArea().setText(text);
                }
            });
        }
    }
}
