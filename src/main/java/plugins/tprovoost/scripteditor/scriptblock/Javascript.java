/*
 * Copyright 2010-2015 Institut Pasteur.
 * 
 * This file is part of Icy.
 * 
 * Icy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Icy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Icy. If not, see <http://www.gnu.org/licenses/>.
 */
package plugins.tprovoost.scripteditor.scriptblock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

import javax.script.ScriptException;

import org.fife.ui.autocomplete.ParameterizedCompletion.Parameter;

import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarMutable;
import plugins.tprovoost.scripteditor.completion.IcyCompletionProvider;
import plugins.tprovoost.scripteditor.completion.types.ScriptFunctionCompletion;
import plugins.tprovoost.scripteditor.completion.types.ScriptFunctionCompletion.BindingFunction;
import plugins.tprovoost.scripteditor.scriptblock.var.VarJavaScriptScript;
import plugins.tprovoost.scripteditor.scriptinghandlers.ScriptEngine;

/**
 * This block allows to define a JavaScript script using multiple inputs from
 * other blocks and produce multiple results when executed.
 * 
 * It is possible to specify multiple inputs using the "Add Input" button. It is
 * also possible to add several outputs by using the "Add Output" button.
 * 
 * Please check the online documentation for more information.
 * 
 * @author Thomas Provoost
 * @author Daniel Gonzalez
 */
public class Javascript extends ScriptBlock {

	public Javascript() {
		super(new VarJavaScriptScript("", "// Click on the button\n// to edit in a frame.\n\noutput0 = input0 * 2"));
	}

	@Override
	protected void installEngineMethods(ScriptEngine engine, ArrayList<Method> methods) {
		// hardcoded functions, to remove in the future
		try {
			engine.eval(
					"function getSequence() { return Packages.icy.main.Icy.getMainInterface().getFocusedSequence() }");
		} catch (ScriptException e1) {
		}
		try {
			engine.eval("function getImage() { return Packages.icy.main.Icy.getMainInterface().getFocusedImage(); }");
		} catch (ScriptException e1) {
		}
		try {
			engine.eval("gui = Packages.icy.main.Icy.getMainInterface()");
		} catch (ScriptException e1) {
		}

		for (Method method : methods) {
			// is it an annotated with BindingFunction?
			BindingFunction blockFunction = method.getAnnotation(BindingFunction.class);
			if (blockFunction == null)
				continue;
			// Generate the function for the provider
			ArrayList<Parameter> fParams = new ArrayList<Parameter>();
			Class<?>[] paramTypes = method.getParameterTypes();

			// get the parameters
			String params = "";
			String functionName = blockFunction.value();
			// get the parameters
			for (int i = 0; i < paramTypes.length; ++i) {
				fParams.add(new Parameter(IcyCompletionProvider.getType(paramTypes[i], true), "arg" + i));
				params += ",arg" + i;
			}
			if (params.length() > 0)
				params = params.substring(1);

			// the object for the provider
			ScriptFunctionCompletion sfc;
			if (Modifier.isStatic(method.getModifiers()))
				sfc = new ScriptFunctionCompletion(null, functionName, method);
			else
				sfc = new ScriptFunctionCompletion(null, method.getName(), method);

			try {
				// FIXME?
				// if (engine instanceof RhinoScriptEngine)
				// {
				if (method.getReturnType() == void.class) {
					engine.eval("function " + functionName + " (" + params + ") {\n\t" + sfc.getMethodCall() + "\n}");
				} else {
					engine.eval("function " + functionName + " (" + params + ") {\n\treturn " + sfc.getMethodCall()
							+ "\n}");
				}
				// }
			} catch (ScriptException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void injectInputVariables(ScriptEngine engine) {
		for (Var<?> var : getVarScript().getInputVariables()) {
			Object value = var.getValue();
			String name = var.getName();
			if (name.contains("input")) {
				// For another language, remove this, or use the right one.
				value = JSScriptBlock.transformInputForScript(value);

				// put in the engine the value.
				engine.put(name, value);
			}
		}

	}

	@Override
	protected void setOutputVariables(ScriptEngine engine) {
		for (VarMutable output : getVarScript().getOutputVariables()) {
			Object resObject = engine.get(output.getName());
			output.setValue(JSScriptBlock.transformScriptOutput(resObject));
		}

	}

}
