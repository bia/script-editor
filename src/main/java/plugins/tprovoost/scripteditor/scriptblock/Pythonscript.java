/*
 * Copyright 2010-2015 Institut Pasteur.
 * 
 * This file is part of Icy.
 * 
 * Icy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Icy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Icy. If not, see <http://www.gnu.org/licenses/>.
 */
package plugins.tprovoost.scripteditor.scriptblock;

import java.lang.reflect.Method;
import java.util.ArrayList;

import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarMutable;
import plugins.tprovoost.scripteditor.scriptblock.var.VarPythonScript;
import plugins.tprovoost.scripteditor.scriptinghandlers.ScriptEngine;


/**
 * This block allows to define a Python script using multiple inputs from
 * other blocks and produce multiple results when executed.
 * 
 * It is possible to specify multiple inputs using the "Add Input" button. It is
 * also possible to add several outputs by using the "Add Output" button.
 * 
 * Please check the online documentation for more information.
 * 
 * @author Thomas Provoost
 * @author Daniel Gonzalez
 */
public class Pythonscript extends ScriptBlock {

	public Pythonscript() {
		super(new VarPythonScript("", "# Click on the button\n# to edit in a frame.\n\noutput0 = input0 * 2"));
	}

	@Override
	protected void installEngineMethods(ScriptEngine engine, ArrayList<Method> methods) {
		// TODO nothing for now?

	}

	@Override
	protected void injectInputVariables(ScriptEngine engine) {
		for (Var<?> var : getVarScript().getInputVariables()) {
			Object value = var.getValue();
			String name = var.getName();
			if (name.contains("input")) {
				// For another language, remove this, or use the right one.
				value = PythonScriptBlock.transformInputForScript(value);

				// put in the engine the value.
				engine.put(name, value);
			}
		}
	}

	@Override
	protected void setOutputVariables(ScriptEngine engine) {
		for (VarMutable output : getVarScript().getOutputVariables()) {
			Object resObject = engine.get(output.getName());
			output.setValue(PythonScriptBlock.transformScriptOutput(resObject));
		}
	}

}
