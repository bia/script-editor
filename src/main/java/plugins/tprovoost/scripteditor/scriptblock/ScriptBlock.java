/**
 * 
 */
package plugins.tprovoost.scripteditor.scriptblock;

import java.io.File;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import icy.plugin.abstract_.Plugin;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.blocks.util.VarListListener;
import plugins.adufour.vars.gui.model.TypeSelectionModel;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.lang.VarTrigger;
import plugins.adufour.vars.util.TypeChangeListener;
import plugins.adufour.vars.util.VarReferencingPolicy;
import plugins.tprovoost.scripteditor.scriptblock.script.ScriptType;
import plugins.tprovoost.scripteditor.scriptblock.var.VarScript;
import plugins.tprovoost.scripteditor.scriptinghandlers.ScriptEngine;
import plugins.tprovoost.scripteditor.scriptinghandlers.ScriptEngineHandler;

/**
 * This is the base class for the scripting blocks. Currently supporting two
 * languages: JavaScript ({@link Javascript}) and Python({@link Pythonscript}).
 * These two blocks inherit from this class to perform common tasks of a
 * scripting protocol block.
 * 
 * @author Daniel Gonzalez
 */
public abstract class ScriptBlock extends Plugin implements Block, VarListListener, TypeChangeListener {

	private List<String> languagesInstalled;

	private VarScript script;

	private VarList inputMap;
	private int inputVarIdx = 0;
	private VarTrigger inputVarTrigger;

	private VarList outputMap;
	private int outputVarIdx = 0;
	private VarTrigger outputVarTrigger;

	private final TypeSelectionModel typeSelector = new TypeSelectionModel(
			new Class<?>[] { Object.class, Object[].class, Sequence.class, ROI[].class, Integer.class, Double.class,
					int[].class, double[].class, String.class, File.class, File[].class });

	public ScriptBlock(VarScript script) {
		this.script = script;
	}

	protected VarScript getVarScript() {
		return script;
	}

	@Override
	public void declareInput(VarList inputMap) {
		languagesInstalled = new ArrayList<String>();
		for (ScriptEngineFactory f : ScriptEngineHandler.getEngineManager().getEngineFactories()) {
			languagesInstalled.add(ScriptEngineHandler.getLanguageName(f));
		}

		if (this.inputMap == null)
			this.inputMap = inputMap;

		script.setReferencingPolicy(VarReferencingPolicy.NONE);
		createAddInputVarTrigger();
		createAddOutputVarTrigger();

		inputMap.addVarListListener(this);

		inputMap.add("Script", script);
		inputMap.add("Add Input", inputVarTrigger);
		inputMap.add("Add output", outputVarTrigger);
		VarMutable firstInputVariable = createInputVariable(inputVarIdx);
		inputMap.add(firstInputVariable.getName(), firstInputVariable);
	}

	private void createAddInputVarTrigger() {
		inputVarTrigger = new VarTrigger("Add Input", new VarTrigger.TriggerListener() {

			@Override
			public void valueChanged(Var<Integer> source, Integer oldValue, Integer newValue) {
				registerVariables();
			}

			@Override
			public void referenceChanged(Var<Integer> source, Var<? extends Integer> oldReference,
					Var<? extends Integer> newReference) {
				registerVariables();
			}

			@Override
			public void triggered(VarTrigger source) {
				VarMutable newVariable = createInputVariable(inputVarIdx);
				inputMap.addRuntimeVariable("" + newVariable.hashCode(), newVariable);
				registerVariables();
			}
		});
		inputVarTrigger.setReferencingPolicy(VarReferencingPolicy.NONE);
	}

	private void createAddOutputVarTrigger() {
		outputVarTrigger = new VarTrigger("Add Output", new VarTrigger.TriggerListener() {

			@Override
			public void valueChanged(Var<Integer> source, Integer oldValue, Integer newValue) {
				registerVariables();
			}

			@Override
			public void referenceChanged(Var<Integer> source, Var<? extends Integer> oldReference,
					Var<? extends Integer> newReference) {
				registerVariables();
			}

			@Override
			public void triggered(VarTrigger source) {
				VarMutable newVariable = createOutputVariable(outputVarIdx);
				outputMap.addRuntimeVariable("" + newVariable.hashCode(), newVariable);
				registerVariables();
			}
		});
		outputVarTrigger.setReferencingPolicy(VarReferencingPolicy.NONE);
	}

	private VarMutable createInputVariable(int varIdx) {
		VarMutable newVariable = new VarMutable("input" + varIdx, Object.class);
		setupVariable(newVariable);
		return newVariable;
	}

	@Override
	public void declareOutput(VarList outputMap) {
		if (this.outputMap == null)
			this.outputMap = outputMap;

		outputMap.addVarListListener(this);

		VarMutable firstOutputVariable = createOutputVariable(outputVarIdx);
		outputMap.add(firstOutputVariable.getName(), firstOutputVariable);
		registerVariables();
	}

	private VarMutable createOutputVariable(int varIdx) {
		VarMutable newVariable = new VarMutable("output" + varIdx, Object.class);
		setupVariable(newVariable);
		return newVariable;
	}

	private void setupVariable(VarMutable newVariable) {
		newVariable.setDefaultEditorModel(typeSelector);
		newVariable.addTypeChangeListener(this);
	}

	@Override
	public void variableAdded(VarList list, Var<?> variable) {
		if (list == inputMap && variable != script && variable != inputVarTrigger && variable != outputVarTrigger)
			inputVarIdx++;
		else if (list == outputMap)
			outputVarIdx++;

		if (list.isRuntimeVariable(variable) && variable instanceof VarMutable) {
			VarMutable mutable = (VarMutable) variable;
			Class<?> type = mutable.getType();
			mutable.setDefaultEditorModel(typeSelector);
			mutable.addTypeChangeListener(this);
			mutable.setType(type);
		}
	}

	@Override
	public void variableRemoved(VarList list, Var<?> variable) {
		if (list.isRuntimeVariable(variable) && variable instanceof VarMutable)
			((VarMutable) variable).removeTypeChangeListener(this);
		registerVariables();
	}

	@Override
	public void typeChanged(Object source, Class<?> oldType, Class<?> newType) {
		registerVariables();
	}

	private void registerVariables() {
		Set<VarMutable> inputVars = new HashSet<VarMutable>();
		Set<VarMutable> outputVars = new HashSet<VarMutable>();
		if (inputMap != null)
			for (Var<?> v : inputMap) {
				if (v instanceof VarMutable || inputMap.isRuntimeVariable(v)) {
					inputVars.add((VarMutable) v);
				}
			}
		if (outputMap != null)
			for (Var<?> v : outputMap) {
				if (v instanceof VarMutable || outputMap.isRuntimeVariable(v)) {
					outputVars.add((VarMutable) v);
				}
			}

		script.setupInputVariables(inputVars);
		script.setupOutputVariables(outputVars);
	}

	@Override
	public void run() {
		ScriptEngine engine = createEngine();
		injectInputVariables(engine);
		try {
			evaluateCode(engine, script.getCode());
		} catch (ScriptException e) {
			e.printStackTrace();
			throw new IcyHandledException(e.getMessage(), e);
		}
		setOutputVariables(engine);
	}

	private ScriptEngine createEngine() {
		ScriptEngine oldEngine = ScriptEngineHandler.getEngine(getScriptType().toString());
		if (oldEngine != null) {
			// retrieve the methods known to the old engine to transfert them to the new
			// engine
			ScriptEngineHandler engineHandler = ScriptEngineHandler.getEngineHandler(oldEngine);
			ArrayList<Method> functions = engineHandler.getFunctions();

			// unregister the old engine (will do the housekeeping)
			ScriptEngineHandler.disposeEngine(oldEngine);

			// create a new engine
			String newEngineType = oldEngine.getName();
			ScriptEngine newEngine = ScriptEngineHandler.getEngine(newEngineType, true);
			installEngineMethods(newEngine, functions);

			newEngine.setWriter(new PrintWriter(System.out));
			newEngine.setErrorWriter(new PrintWriter(System.err));

			return newEngine;
		} else {
			// Failed to retrieve the current engine
			return null;
		}
	}

	private ScriptType getScriptType() {
		return script.getValue().getScriptType();
	}

	protected abstract void installEngineMethods(ScriptEngine engine, ArrayList<Method> methods);

	protected abstract void injectInputVariables(ScriptEngine engine);

	private void evaluateCode(ScriptEngine engine, String code) throws ScriptException {
		engine.eval(script.getCode());

	}

	protected abstract void setOutputVariables(ScriptEngine engine);

}
