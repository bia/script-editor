package plugins.tprovoost.scripteditor.scriptblock.editor;

import plugins.tprovoost.scripteditor.scriptblock.var.VarJavaScriptScript;
import plugins.tprovoost.scripteditor.scriptblock.var.VarPythonScript;
import plugins.tprovoost.scripteditor.scriptblock.var.VarScript;

public class DefaultScriptVarEditorFactory {

	private static DefaultScriptVarEditorFactory defaultFactory = new  DefaultScriptVarEditorFactory();;
	
	public static DefaultScriptVarEditorFactory getDefaultFactory() {
		return defaultFactory;
	}

	public VarScriptEditor createScriptEditor(VarScript var) {
		switch (var.getValue().getScriptType()) {
		case JAVASCRIPT:
			return new JavaScriptScriptVarEditor((VarJavaScriptScript)var);
		case PYTHON:
			return new PythonScriptVarEditor((VarPythonScript)var);
		default:
			throw new RuntimeException("Unsupported script type: " + var.getValue().getScriptType());
		}
	}
}
