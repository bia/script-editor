package plugins.tprovoost.scripteditor.scriptblock.editor;

import plugins.tprovoost.scripteditor.scriptblock.var.VarJavaScriptScript;

public class JavaScriptScriptVarEditor extends VarScriptEditor {

	public JavaScriptScriptVarEditor(VarJavaScriptScript variable) {
		super(variable);
	}
}
