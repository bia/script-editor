package plugins.tprovoost.scripteditor.scriptblock.editor;

import plugins.tprovoost.scripteditor.scriptblock.var.VarPythonScript;

public class PythonScriptVarEditor extends VarScriptEditor {

	public PythonScriptVarEditor(VarPythonScript variable) {
		super(variable); // Calls createEditorComponent
	}
}
