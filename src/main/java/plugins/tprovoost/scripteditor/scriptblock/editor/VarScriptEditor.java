package plugins.tprovoost.scripteditor.scriptblock.editor;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import icy.gui.component.button.IcyButton;
import icy.gui.frame.IcyFrame;
import icy.gui.frame.IcyFrameAdapter;
import icy.gui.frame.IcyFrameEvent;
import icy.gui.frame.IcyFrameListener;
import icy.resource.icon.IcyIcon;
import icy.system.thread.ThreadUtil;
import plugins.adufour.vars.gui.swing.SwingVarEditor;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarMutable;
import plugins.tprovoost.scripteditor.gui.ScriptingPanel;
import plugins.tprovoost.scripteditor.scriptblock.script.Script;
import plugins.tprovoost.scripteditor.scriptblock.var.VarScript;
import plugins.tprovoost.scripteditor.scriptinghandlers.ScriptVariable;
import plugins.tprovoost.scripteditor.scriptinghandlers.ScriptingHandler;
import plugins.tprovoost.scripteditor.scriptinghandlers.VariableType;

public abstract class VarScriptEditor extends SwingVarEditor<Script> {
	private ScriptingPanel panelIn;
	private ScriptingPanel panelOut;

	private IcyFrame frame;
	private IcyFrameListener frameListener;

	public VarScriptEditor(VarScript variable) {
		super(variable); // Calls createEditorComponent
	}

	@Override
	public VarScript getVariable() {
		return (VarScript) super.getVariable();
	}

	@Override
	protected JComponent createEditorComponent() {
		setNameVisible(false);
		setComponentResizeable(true);
		JPanel component = buildComponentPanel();
		final Set<VarMutable> inputVariables = getVariable().getInputVariables();
		final Set<VarMutable> outputVariables = getVariable().getOutputVariables();
		ThreadUtil.invokeLater(new Runnable() {
			@Override
			public void run() {
				setupVariables(inputVariables, outputVariables);
				panelIn.getScriptHandler().interpret(false);
				panelOut.getScriptHandler().interpret(false);
			}
		});
		return component;
	}

	private JPanel buildComponentPanel() {
		JPanel componentPanel = new JPanel(new BorderLayout());
		componentPanel.setOpaque(false);
		componentPanel.add(buildInternalEditorPanel(), BorderLayout.CENTER);
		componentPanel.add(buildInternalEastPanel(), BorderLayout.EAST);
		return componentPanel;
	}

	private JPanel buildInternalEditorPanel() {
		panelIn = new ScriptingPanel("Internal Editor", getEditorScriptType());
		panelIn.setText(getVariable().getCode());
		panelIn.getTextArea().setCaretPosition(0);
		panelIn.getTextArea().getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				getVariable().getValue().updateCode(panelIn.getTextArea().getText());
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				getVariable().getValue().updateCode(panelIn.getTextArea().getText());
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
			}
		});
		return panelIn;
	}

	private String getEditorScriptType() {
		switch (getVariable().getValue().getScriptType()) {
		case JAVASCRIPT:
			return "JavaScript";
		case PYTHON:
			return "Python";
		default:
			throw new RuntimeException("Unsupported script type: " + getVariable().getValue().getScriptType());
		}
	}

	private JPanel buildInternalEastPanel() {
		JPanel eastPanel = new JPanel();
		eastPanel.setOpaque(false);
		eastPanel.setLayout(new BoxLayout(eastPanel, BoxLayout.Y_AXIS));
		eastPanel.add(Box.createVerticalGlue());

		IcyButton editButton = new IcyButton(new IcyIcon("redo.png", 12));
		eastPanel.add(editButton);
		eastPanel.add(Box.createVerticalGlue());

		buildExternalEditorFrame();

		editButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getVariable().updateCode(panelIn.getTextArea().getText());
				panelOut.getTextArea().setText(getVariable().getCode());
				panelOut.getTextArea().setCaretPosition(panelIn.getTextArea().getCaretPosition());
				panelIn.getTextArea().setEnabled(false);
				frame.setVisible(true);
				frame.requestFocus();
			}
		});

		return eastPanel;
	}

	private void buildExternalEditorFrame() {
		frame = new IcyFrame("External Editor", true, true, true, true);
		frame.setSize(500, 500);

		buildExternalPanel();
		frame.setContentPane(panelOut);

		frame.addToDesktopPane();
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.center();

		frameListener = new IcyFrameAdapter() {
			@Override
			public void icyFrameClosing(IcyFrameEvent e) {
				panelIn.getTextArea().setText(panelOut.getTextArea().getText());
				panelIn.getTextArea().setCaretPosition(panelOut.getTextArea().getCaretPosition());
				panelIn.getTextArea().setEnabled(true);
				panelIn.getTextArea().repaint();
			}
		};
	}

	private void buildExternalPanel() {
		panelOut = new ScriptingPanel("External Editor", getEditorScriptType());
		panelOut.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		panelOut.getTextArea().setCaretPosition(0);
		panelOut.getTextArea().setText(panelIn.getTextArea().getText());
	}

	@Override
	public double getComponentVerticalResizeFactor() {
		return 1d;
	}

	@Override
	protected void activateListeners() {
		frame.addFrameListener(frameListener);
	}

	@Override
	protected void deactivateListeners() {
		frame.removeFrameListener(frameListener);
		frame.close();
	}

	@Override
	protected void updateInterfaceValue() {
		panelIn.getTextArea().setText(getVariable().getValue().getCode());
		panelOut.getTextArea().setText(getVariable().getValue().getCode());
		setupVariables(getVariable().getInputVariables(), getVariable().getOutputVariables());
	}

	public void setupVariables(Set<VarMutable> inputVariables, Set<VarMutable> outputVariables) {
		ScriptingHandler inHandler = panelIn.getScriptHandler();
		ScriptingHandler outHandler = panelOut.getScriptHandler();
		setupVariables(inputVariables, outputVariables, inHandler);
		setupVariables(inputVariables, outputVariables, outHandler);
	}

	private void setupVariables(Set<VarMutable> inputVariables, Set<VarMutable> outputVariables,
			final ScriptingHandler handler) {
		HashMap<String, ScriptVariable> handlerVariables = handler.getExternalVariables();

		handlerVariables.clear();
		for (Var<?> inputVar : inputVariables) {
			handlerVariables.put(inputVar.getName(), new ScriptVariable(new VariableType(inputVar.getType())));
		}
		for (Var<?> outputVar : outputVariables) {
			handlerVariables.put(outputVar.getName(), new ScriptVariable(new VariableType(outputVar.getType())));
		}
		ThreadUtil.invokeLater(new Runnable() {

			@Override
			public void run() {
				handler.interpret(false);
			}
		});
	}
}
