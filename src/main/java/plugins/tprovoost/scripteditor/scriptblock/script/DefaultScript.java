package plugins.tprovoost.scripteditor.scriptblock.script;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import plugins.adufour.vars.lang.VarMutable;

public abstract class DefaultScript implements Script {

	private String code;
	private Set<VarMutable> variables;
	
	public DefaultScript() {
		this("");
	}
	
	public DefaultScript(String code) {
		this(code, Collections.<VarMutable>emptyList());
	}

	public DefaultScript(String code, Collection<VarMutable> variables) {
		this.code = code;
		this.variables = new HashSet<VarMutable>(variables);
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public Set<VarMutable> getVariables() {
		return Collections.unmodifiableSet(variables);
	}

	@Override
	public void updateCode(String newCode) {
		this.code = newCode;
	}

	@Override
	public void addVariable(VarMutable variable) {
		variables.add(variable);
	}

	@Override
	public void addVariables(Collection<VarMutable> variables) {
		this.variables.addAll(variables);
	}

	@Override
	public void removeVariable(VarMutable variable) {
		variables.remove(variable);
	}

	@Override
	public void removeVariables(Collection<VarMutable> variables) {
		this.variables.removeAll(variables);
	}
	
	@Override
	public String toString() {
		return getCode();
	}

}
