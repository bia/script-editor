package plugins.tprovoost.scripteditor.scriptblock.script;

import java.util.Collection;
import java.util.Collections;

import plugins.adufour.vars.lang.VarMutable;

public class JavaScriptScript extends DefaultScript {

	public JavaScriptScript() {
		this("");
	}

	public JavaScriptScript(String code) {
		this(code, Collections.<VarMutable>emptyList());
	}

	public JavaScriptScript(String code, Collection<VarMutable> variables) {
		super(code, variables);
	}

	@Override
	public ScriptType getScriptType() {
		return ScriptType.JAVASCRIPT;
	}

}
