package plugins.tprovoost.scripteditor.scriptblock.script;

import java.util.Collection;
import java.util.Collections;

import plugins.adufour.vars.lang.VarMutable;

public class PythonScript extends DefaultScript {

	public PythonScript() {
		this("");
	}

	public PythonScript(String code) {
		this(code, Collections.<VarMutable>emptyList());
	}

	public PythonScript(String code, Collection<VarMutable> variables) {
		super(code, variables);
	}

	@Override
	public ScriptType getScriptType() {
		return ScriptType.PYTHON;
	}

}
