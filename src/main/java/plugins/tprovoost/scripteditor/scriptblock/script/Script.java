package plugins.tprovoost.scripteditor.scriptblock.script;

import java.util.Collection;
import java.util.Set;

import plugins.adufour.vars.lang.VarMutable;

public interface Script {
	ScriptType getScriptType();

	String getCode();

	Set<VarMutable> getVariables();

	void updateCode(String newCode);

	void addVariable(VarMutable variable);

	void addVariables(Collection<VarMutable> inputVariables);

	void removeVariable(VarMutable variable);

	void removeVariables(Collection<VarMutable> inputVariables);
}
