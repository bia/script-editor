package plugins.tprovoost.scripteditor.scriptblock.script;

public enum ScriptType {
	JAVASCRIPT("javascript"), PYTHON("python");
	
	private String stringVal;
	
	ScriptType(String stringVal) {
		this.stringVal = stringVal;
	}
	
	@Override
	public String toString() {
		return stringVal;
	}
}
