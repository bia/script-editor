package plugins.tprovoost.scripteditor.scriptblock.var;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.util.VarListener;
import plugins.tprovoost.scripteditor.scriptblock.script.JavaScriptScript;
import plugins.tprovoost.scripteditor.scriptblock.script.Script;

public class VarJavaScriptScript extends VarScript {

	public VarJavaScriptScript(String name, String defaultValue) {
		this(name, defaultValue, Collections.<VarMutable>emptySet(), Collections.<VarMutable>emptySet());
	}

	public VarJavaScriptScript(String name, String defaultValue, Collection<VarMutable> inputVariables,
			Collection<VarMutable> outputVariables) throws NullPointerException {
		this(name, defaultValue, inputVariables, outputVariables, null);
	}

	public VarJavaScriptScript(String name, String defaultValue, VarListener<Script> defaultListener)
			throws NullPointerException {
		this(name, defaultValue, Collections.<VarMutable>emptyList(), Collections.<VarMutable>emptyList(), defaultListener);
	}

	public VarJavaScriptScript(String name, String defaultValue, Collection<VarMutable> inputVariables,
			Collection<VarMutable> outputVariables, VarListener<Script> defaultListener)
			throws NullPointerException {
		super(name, new JavaScriptScript(filterNullValue(defaultValue)), defaultListener);
		this.inputVariables = new HashSet<VarMutable>(inputVariables);
		this.outputVariables = new HashSet<VarMutable>(outputVariables);
	}

	private static String filterNullValue(String defaultValue) {
		return defaultValue == null ? "" : defaultValue;
	}

	@Override
	public JavaScriptScript parse(String text) {
		return new JavaScriptScript(text);
	}
}
