package plugins.tprovoost.scripteditor.scriptblock.var;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import plugins.adufour.vars.gui.VarEditor;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarMutable;
import plugins.adufour.vars.util.VarListener;
import plugins.tprovoost.scripteditor.scriptblock.editor.DefaultScriptVarEditorFactory;
import plugins.tprovoost.scripteditor.scriptblock.editor.VarScriptEditor;
import plugins.tprovoost.scripteditor.scriptblock.script.Script;

public abstract class VarScript extends Var<Script> {

	Set<VarMutable> inputVariables;
	Set<VarMutable> outputVariables;

	public VarScript(String name, Script defaultValue) {
		this(name, defaultValue, Collections.<VarMutable>emptySet(), Collections.<VarMutable>emptySet());
	}

	public VarScript(String name, Script defaultValue, Collection<VarMutable> inputVariables,
			Collection<VarMutable> outputVariables) throws NullPointerException {
		this(name, defaultValue, inputVariables, outputVariables, null);
	}

	public VarScript(String name, Script defaultValue, VarListener<Script> defaultListener)
			throws NullPointerException {
		this(name, defaultValue, Collections.<VarMutable>emptyList(), Collections.<VarMutable>emptyList(), defaultListener);
	}

	public VarScript(String name, Script defaultValue, Collection<VarMutable> inputVariables,
			Collection<VarMutable> outputVariables, VarListener<Script> defaultListener)
			throws NullPointerException {
		super(name, defaultValue, defaultListener);
		setValue(defaultValue);
		this.inputVariables = new HashSet<VarMutable>(inputVariables);
		this.outputVariables = new HashSet<VarMutable>(outputVariables);
	}

	public String getCode() {
		return getValue().getCode();
	}

	public Set<VarMutable> getVariables() {
		return getValue().getVariables();
	}

	public Set<VarMutable> getInputVariables() {
		return Collections.unmodifiableSet(inputVariables);
	}

	public Set<VarMutable> getOutputVariables() {
		return Collections.unmodifiableSet(outputVariables);
	}
	
	@Override
	public void setValue(Script newValue) throws IllegalArgumentException {
		if (newValue != null && newValue.getCode() != null)
			super.setValue(newValue);
	}

	public void updateCode(String newCode) {
		if (newCode != null) {
			getValue().updateCode(newCode);
			fireVariableChanged(getValue(), getValue());
		}
	}

	public void setupInputVariables(Collection<VarMutable> variables) {
		getValue().removeVariables(getInputVariables());
		inputVariables.clear();
		inputVariables.addAll(variables);
		getValue().addVariables(getInputVariables());

	}

	public void setupOutputVariables(Collection<VarMutable> variables) {
		getValue().removeVariables(getOutputVariables());
		outputVariables.clear();
		outputVariables.addAll(variables);
		getValue().addVariables(getOutputVariables());

	}

	public void addInputVariable(VarMutable variable) {
		getValue().addVariable(variable);
		inputVariables.add(variable);
		fireVariableChanged(getValue(), getValue());
	}

	public void addOutputVariable(VarMutable variable) {
		getValue().addVariable(variable);
		outputVariables.add(variable);
		fireVariableChanged(getValue(), getValue());
	}

	public void removeInputVariable(VarMutable variable) {
		getValue().removeVariable(variable);
		inputVariables.remove(variable);
		fireVariableChanged(getValue(), getValue());
	}

	public void removeOutputVariable(VarMutable variable) {
		getValue().removeVariable(variable);
		outputVariables.remove(variable);
		fireVariableChanged(getValue(), getValue());
	}

	@Override
	public VarEditor<Script> createVarEditor() {
		DefaultScriptVarEditorFactory factory = DefaultScriptVarEditorFactory.getDefaultFactory();
		VarScriptEditor editor = factory.createScriptEditor(this);
		return editor;
	}

//	private Set<VarMutableScript> inputVariables;
//	private Set<VarMutableScript> outputVariables;
//
//	public VarScript(String name, String defaultValue) {
//		super(name, defaultValue);
//		inputVariables = new HashSet<>();
//		outputVariables = new HashSet<>();
//	}
//
//	public abstract ScriptType getScriptType();
//
//	@Override
//	public synchronized VarScriptEditor createVarEditor() {
//		DefaultScriptVarEditorFactory factory = DefaultScriptVarEditorFactory.getDefaultFactory();
//		VarScriptEditor editor = factory.createScriptEditor(this, getScriptType());
//		editor.setupVariables(inputVariables, outputVariables);
//		return editor;
//	}
//	
//	public void addInputVariable(VarMutableScript variable) {
//		inputVariables.add(variable);
//		super.fireVariableChanged(getValue(), getValue());
//	}
//
//	@Override
//	public boolean saveToXML(Node node) throws UnsupportedOperationException {
//		boolean res = super.saveToXML(node);
//		return res;
//	}
//
//	@Override
//	public boolean loadFromXML(Node node) {
//		boolean res = super.loadFromXML(node);
//		return res;
//	}
}
