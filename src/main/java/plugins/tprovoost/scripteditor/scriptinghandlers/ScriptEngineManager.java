package plugins.tprovoost.scripteditor.scriptinghandlers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.SimpleBindings;

import icy.plugin.PluginDescriptor;
import icy.plugin.PluginLauncher;
import icy.plugin.PluginLoader;
import icy.plugin.interface_.PluginScriptFactory;

public class ScriptEngineManager
{
    /** Set of script engine factories discovered. */
    private HashSet<ScriptEngineFactory> engineSpis = new HashSet<>();

    /** Map of engine name to script engine factory. */
    private HashMap<String, ScriptEngineFactory> nameAssociations = new HashMap<>();

    /** Map of script file extension to script engine factory. */
    private HashMap<String, ScriptEngineFactory> extensionAssociations = new HashMap<>();

    /** Map of script script MIME type to script engine factory. */
    private HashMap<String, ScriptEngineFactory> mimeTypeAssociations = new HashMap<>();

    /** Global bindings associated with script engines created by this manager. */
    private Bindings globalScope = new SimpleBindings();

    public ScriptEngineManager()
    {
        super();

        for (PluginDescriptor plugin : PluginLoader.getPlugins(PluginScriptFactory.class, true, false, false))
        {
            try
            {
                // create plugin instance
                PluginScriptFactory psf = (PluginScriptFactory) PluginLauncher.create(plugin);
                // get the factory and add it to the list
                engineSpis.add(psf.getScriptEngineFactory());
            }
            catch (Throwable t)
            {
                System.err.println("Error while retrieving ScriptEngineFactory from " + plugin.getName() + ":");
                System.err.println(t.getMessage());
            }
        }
    }

    /**
     * <code>setBindings</code> stores the specified <code>Bindings</code>
     * in the <code>globalScope</code> field. ScriptEngineManager sets this
     * <code>Bindings</code> as global bindings for <code>ScriptEngine</code>
     * objects created by it.
     *
     * @param bindings
     *        The specified <code>Bindings</code>
     * @throws IllegalArgumentException
     *         if bindings is null.
     */
    public void setBindings(Bindings bindings)
    {
        if (bindings == null)
        {
            throw new IllegalArgumentException("Global scope cannot be null.");
        }

        globalScope = bindings;
    }

    /**
     * <code>getBindings</code> returns the value of the <code>globalScope</code> field.
     * ScriptEngineManager sets this <code>Bindings</code> as global bindings for
     * <code>ScriptEngine</code> objects created by it.
     *
     * @return The globalScope field.
     */
    public Bindings getBindings()
    {
        return globalScope;
    }

    /**
     * Sets the specified key/value pair in the Global Scope.
     * 
     * @param key
     *        Key to set
     * @param value
     *        Value to set.
     * @throws NullPointerException
     *         if key is null.
     * @throws IllegalArgumentException
     *         if key is empty string.
     */
    public void put(String key, Object value)
    {
        globalScope.put(key, value);
    }

    /**
     * Gets the value for the specified key in the Global Scope
     * 
     * @param key
     *        The key whose value is to be returned.
     * @return The value for the specified key.
     */
    public Object get(String key)
    {
        return globalScope.get(key);
    }

    /**
     * Looks up and creates a <code>ScriptEngine</code> for a given name.
     * The algorithm first searches for a <code>ScriptEngineFactory</code> that has been
     * registered as a handler for the specified name using the <code>registerEngineName</code>
     * method.
     * <br>
     * <br>
     * If one is not found, it searches the set of <code>ScriptEngineFactory</code> instances
     * stored by the constructor for one with the specified name. If a <code>ScriptEngineFactory</code>
     * is found by either method, it is used to create instance of <code>ScriptEngine</code>.
     * 
     * @param shortName
     *        The short name of the <code>ScriptEngine</code> implementation.
     *        returned by the <code>getNames</code> method of its <code>ScriptEngineFactory</code>.
     * @return A <code>ScriptEngine</code> created by the factory located in the search. Returns null
     *         if no such factory was found. The <code>ScriptEngineManager</code> sets its own <code>globalScope</code>
     *         <code>Bindings</code> as the <code>GLOBAL_SCOPE</code> <code>Bindings</code> of the newly
     *         created <code>ScriptEngine</code>.
     * @throws NullPointerException
     *         if shortName is null.
     */
    public javax.script.ScriptEngine getEngineByName(String shortName)
    {
        if (shortName == null)
            throw new NullPointerException();
        // look for registered name first
        Object obj;
        if (null != (obj = nameAssociations.get(shortName)))
        {
            ScriptEngineFactory spi = (ScriptEngineFactory) obj;
            try
            {
                ScriptEngine engine = spi.getScriptEngine();
                engine.setBindings(getBindings(), ScriptContext.GLOBAL_SCOPE);
                return engine;
            }
            catch (Exception exp)
            {
                debugPrint(exp);
            }
        }

        for (ScriptEngineFactory spi : engineSpis)
        {
            List<String> names = null;
            try
            {
                names = spi.getNames();
            }
            catch (Exception exp)
            {
                debugPrint(exp);
            }

            if (names != null)
            {
                for (String name : names)
                {
                    if (shortName.equals(name))
                    {
                        try
                        {
                            ScriptEngine engine = spi.getScriptEngine();
                            engine.setBindings(getBindings(), ScriptContext.GLOBAL_SCOPE);
                            return engine;
                        }
                        catch (Exception exp)
                        {
                            debugPrint(exp);
                        }
                    }
                }
            }
        }

        return null;
    }

    /**
     * Look up and create a <code>ScriptEngine</code> for a given extension. The algorithm
     * used by <code>getEngineByName</code> is used except that the search starts
     * by looking for a <code>ScriptEngineFactory</code> registered to handle the
     * given extension using <code>registerEngineExtension</code>.
     * 
     * @param extension
     *        The given extension
     * @return The engine to handle scripts with this extension. Returns <code>null</code>
     *         if not found.
     * @throws NullPointerException
     *         if extension is null.
     */
    public ScriptEngine getEngineByExtension(String extension)
    {
        if (extension == null)
            throw new NullPointerException();
        // look for registered extension first
        Object obj;
        if (null != (obj = extensionAssociations.get(extension)))
        {
            ScriptEngineFactory spi = (ScriptEngineFactory) obj;
            try
            {
                ScriptEngine engine = spi.getScriptEngine();
                engine.setBindings(getBindings(), ScriptContext.GLOBAL_SCOPE);
                return engine;
            }
            catch (Exception exp)
            {
                debugPrint(exp);
            }
        }

        for (ScriptEngineFactory spi : engineSpis)
        {
            List<String> exts = null;
            try
            {
                exts = spi.getExtensions();
            }
            catch (Exception exp)
            {
                debugPrint(exp);
            }
            if (exts == null)
                continue;
            for (String ext : exts)
            {
                if (extension.equals(ext))
                {
                    try
                    {
                        ScriptEngine engine = spi.getScriptEngine();
                        engine.setBindings(getBindings(), ScriptContext.GLOBAL_SCOPE);
                        return engine;
                    }
                    catch (Exception exp)
                    {
                        debugPrint(exp);
                    }
                }
            }
        }
        return null;
    }

    /**
     * Look up and create a <code>ScriptEngine</code> for a given mime type. The algorithm
     * used by <code>getEngineByName</code> is used except that the search starts
     * by looking for a <code>ScriptEngineFactory</code> registered to handle the
     * given mime type using <code>registerEngineMimeType</code>.
     * 
     * @param mimeType
     *        The given mime type
     * @return The engine to handle scripts with this mime type. Returns <code>null</code>
     *         if not found.
     * @throws NullPointerException
     *         if mimeType is null.
     */
    public ScriptEngine getEngineByMimeType(String mimeType)
    {
        if (mimeType == null)
            throw new NullPointerException();
        // look for registered types first
        Object obj;
        if (null != (obj = mimeTypeAssociations.get(mimeType)))
        {
            ScriptEngineFactory spi = (ScriptEngineFactory) obj;
            try
            {
                ScriptEngine engine = spi.getScriptEngine();
                engine.setBindings(getBindings(), ScriptContext.GLOBAL_SCOPE);
                return engine;
            }
            catch (Exception exp)
            {
                debugPrint(exp);
            }
        }

        for (ScriptEngineFactory spi : engineSpis)
        {
            List<String> types = null;
            try
            {
                types = spi.getMimeTypes();
            }
            catch (Exception exp)
            {
                debugPrint(exp);
            }
            if (types == null)
                continue;
            for (String type : types)
            {
                if (mimeType.equals(type))
                {
                    try
                    {
                        ScriptEngine engine = spi.getScriptEngine();
                        engine.setBindings(getBindings(), ScriptContext.GLOBAL_SCOPE);
                        return engine;
                    }
                    catch (Exception exp)
                    {
                        debugPrint(exp);
                    }
                }
            }
        }
        return null;
    }

    /**
     * Returns a list whose elements are instances of all the <code>ScriptEngineFactory</code> classes
     * found by the discovery mechanism.
     * 
     * @return List of all discovered <code>ScriptEngineFactory</code>s.
     */
    public List<ScriptEngineFactory> getEngineFactories()
    {
        List<ScriptEngineFactory> res = new ArrayList<ScriptEngineFactory>(engineSpis.size());
        for (ScriptEngineFactory spi : engineSpis)
        {
            res.add(spi);
        }
        return Collections.unmodifiableList(res);
    }

    /**
     * Registers a <code>ScriptEngineFactory</code> to handle a language
     * name. Overrides any such association found using the Discovery mechanism.
     * 
     * @param name
     *        The name to be associated with the <code>ScriptEngineFactory</code>.
     * @param factory
     *        The class to associate with the given name.
     * @throws NullPointerException
     *         if any of the parameters is null.
     */
    public void registerEngineName(String name, ScriptEngineFactory factory)
    {
        if (name == null || factory == null)
            throw new NullPointerException();
        nameAssociations.put(name, factory);
    }

    /**
     * Registers a <code>ScriptEngineFactory</code> to handle a mime type.
     * Overrides any such association found using the Discovery mechanism.
     *
     * @param type
     *        The mime type to be associated with the
     *        <code>ScriptEngineFactory</code>.
     * @param factory
     *        The class to associate with the given mime type.
     * @throws NullPointerException
     *         if any of the parameters is null.
     */
    public void registerEngineMimeType(String type, ScriptEngineFactory factory)
    {
        if (type == null || factory == null)
            throw new NullPointerException();
        mimeTypeAssociations.put(type, factory);
    }

    /**
     * Registers a <code>ScriptEngineFactory</code> to handle an extension.
     * Overrides any such association found using the Discovery mechanism.
     *
     * @param extension
     *        The extension type to be associated with the
     *        <code>ScriptEngineFactory</code>.
     * @param factory
     *        The class to associate with the given extension.
     * @throws NullPointerException
     *         if any of the parameters is null.
     */
    public void registerEngineExtension(String extension, ScriptEngineFactory factory)
    {
        if (extension == null || factory == null)
            throw new NullPointerException();
        extensionAssociations.put(extension, factory);
    }

    private static void debugPrint(Throwable exp)
    {
        exp.printStackTrace();
    }
}
