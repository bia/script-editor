package plugins.tprovoost.scripteditor.search;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.plugin.interface_.PluginSearchProvider;
import icy.search.SearchResultProducer;
import plugins.tprovoost.scripteditor.main.ScriptEditorPlugin;

public class OnlineScriptProvider extends Plugin implements PluginSearchProvider, PluginBundled
{
    @Override
    public Class<? extends SearchResultProducer> getSearchProviderClass()
    {
        return ScriptProvider.class;
    }

    @Override
    public String getMainPluginClassName()
    {
        return ScriptEditorPlugin.class.getName();
    }
}
